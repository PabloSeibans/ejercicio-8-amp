import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { UsuarioDataI } from 'src/app/interfaces/usuario.interface';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listatarjeta',
  templateUrl: './listatarjeta.component.html',
  styleUrls: ['./listatarjeta.component.css']
})
export class ListatarjetaComponent implements OnInit,AfterViewInit {
  listUsuarios: UsuarioDataI[] = [];
  displayedColumns: string[] = ['usuario', 'target', 'date', 'acciones'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  cargarUsuarios() {
    this.listUsuarios = this._usuarioService.getUsuario();
    this.dataSource = new MatTableDataSource(this.listUsuarios);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  constructor(private _usuarioService: UsuarioService,
              private router: Router) { }

  ngOnInit(): void {
    this.cargarUsuarios();
  }

  ngOnChanges(): void {
    this.cargarUsuarios();
  }

  applyFilter(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarUsuario(usuario: string){
    const opcion = confirm('Estas seguro de eliminar el usuario?');
    if (opcion) {
      console.log(usuario); 
      this._usuarioService.eliminarUsuario(usuario);
      this.cargarUsuarios();
    }
  }


  modificarUsuario(usuario: string){
    console.log(usuario);
    this.router.navigate(['agregartarjeta', usuario]);
  }
}
