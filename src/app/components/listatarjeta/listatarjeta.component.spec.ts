import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListatarjetaComponent } from './listatarjeta.component';

describe('ListatarjetaComponent', () => {
  let component: ListatarjetaComponent;
  let fixture: ComponentFixture<ListatarjetaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListatarjetaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListatarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
