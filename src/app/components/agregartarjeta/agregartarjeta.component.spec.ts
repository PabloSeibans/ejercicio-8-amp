import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregartarjetaComponent } from './agregartarjeta.component';

describe('AgregartarjetaComponent', () => {
  let component: AgregartarjetaComponent;
  let fixture: ComponentFixture<AgregartarjetaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregartarjetaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregartarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
