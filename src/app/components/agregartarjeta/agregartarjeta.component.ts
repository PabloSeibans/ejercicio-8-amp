import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsuarioDataI } from 'src/app/interfaces/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';


@Component({
  selector: 'app-agregartarjeta',
  templateUrl: './agregartarjeta.component.html',
  styleUrls: ['./agregartarjeta.component.css']
})
export class AgregartarjetaComponent implements OnInit {

  form!: FormGroup;
  title: string = 'Agregar Tarjeta';
  submitted: boolean = false;

  adicionar : boolean = true;

  constructor(private fb: FormBuilder,
              private _usuariosService: UsuarioService) {
    this.formulario();
  }

  ngOnInit(): void {
  }

  formulario(): void{
    this.form = this.fb.group({
      usuario: ['', [Validators.required, Validators.minLength(3), Validators.pattern(/^[A-ZÑ\s]+$/)]],
      target: ['', [Validators.required, Validators.minLength(16),Validators.maxLength(16), Validators.pattern(/^[0-9]+$/)]],
      date: ['', [Validators.required, Validators.minLength(5),Validators.maxLength(5), Validators.pattern(/^[0-9\/]+$/)]],
      key: ['', [Validators.required, Validators.maxLength(3), Validators.minLength(3)]]
    });
  }

  get control() {
    return this.form.controls
  }

  agregarUsuario():void {
    console.log(this.form.value);
    
    if(!this.form.valid){
      return;
    }
  
    const user : UsuarioDataI = {
      usuario: this.form.value.usuario,
      target: this.form.value.target,
      date: this.form.value.date,
      key: this.form.value.key
    }

    if(this.adicionar){
      this._usuariosService.agregarUsuario(user);
      this.form.reset();
    } else {
      this._usuariosService.modificarUsuario(user);
      this.form.reset();
    }
  }
}
