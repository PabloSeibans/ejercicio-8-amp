export interface UsuarioDataI {
    usuario: string;
    target: string;
    date: string;
    key: string;
}